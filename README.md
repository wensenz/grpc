# php-grpc-client 简单玩法


## 本机个组件版本 
* PHP               7.1.33
* Composer version  1.7.2
* node              v10.12.0
* protoc            libprotoc 3.11.4


###### 本例用的是pecl安装protobuf & grpc
   ```
    $ pecl install grpc
    $ pecl install protobuf
   ```

## helloWorld 的例子

 - 启动node服务端
   ```
   $ cd examples/node
   $ npm install
   $ cd dynamic_codegen
   $ node greeter_server.js
   ```

 - 客户端调用
   ```
   $ cd examples/php
   $ composer install
   $ ./greeter_proto_gen.sh
   $ ./run_greeter_client.sh
   ```

   
## Routeguide 的例子
 
- 启动node服务端
   ```
   $ cd examples/node/dynamic_codegen/route_guide/
   $ node route_guide_server.js
   ```

 - 客户端调用
   ```
   $ cd ~/myGrpc/examples/php/route_guide 
   $ ./route_guide_proto_gen.sh
   $ ./run_route_guide_client.sh
   ```
 - route详细解释在这[gRPC-PHP]


## Python 作为服务端的例子
有缘再补充



## php-grpc的基础镜像
 - 镜像地址: [docker-grpc-php]
    ```
   grpc_php_plugin 位置
   /github/grpc/bins/opt/grpc_php_plugin
       
    protoc 位置
    /usr/local/bin/protoc
       
    composer.phar 位置
    /github/grpc/examples/php/composer.phar
   ```

###### 由于bins/opt/grpc_php_plugin在mac下面构建生成, 此教程在mac下面测试通过, 未测试其他平台，本例只作为学习笔记



[Node]:https://github.com/grpc/grpc/tree/master/examples/node
[gRPC-PHP]:https://grpc.io/docs/tutorials/basic/php.html
[docker-grpc-php]:https://hub.docker.com/repository/docker/wensen/grpc-php